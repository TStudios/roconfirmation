# RoConfirmation
A ROBLOX Confirmation prompt module
## Documentation
Documentation can be found in the main script, although, it's not that good.

## Setup Instructions
1. Follow step 1 from Update Instructions
2. Insert it into your game
3. Follow the instructions in the `RoConfirmation` ModuleScript
4. You're done

## Update Instructions
### General Update Instructions
1. Download the latest RoConfirmation_[VERSION].rbxm/RoConfirmation_[VERSION].rbxmx from the releases page.
2. Copy the existing config, or remember the content
3. Insert the RoConfirmation_[VERSION].rbxm/RoConfirmation_[VERSION].rbxmx into your game
4. Finished! You updated it
### Specific Version Update Instructions
#### None Yet

## Why isn't the source directly here?
I can't be bothered to make the UI be generated, and I cannot find a module that syncs from ROBLOX, and syncs UIs too.
## Why does this repository exist, if the above is true?
[The Latest Version File](Latest_Stable) provides information to see if an upgrade is possible. If I didn't make this repository, I wouldn't be able to make my project check for updates, as said file wouldn't be available.
## Where is the source?
Check the releases page, and download the RoConfirmation_[VERSION].rbxm/RoConfirmation_[VERSION].rbxmx file. From there, open it in any studio place.
## License
Copyright (c) 2017-2020 0J3_0.

This software is licensed under the AGPL3.0

## Links
| Info | Link 1 | Link 2 | Link 3 | Link 4 |
|---|---|---|---|---|
| AGPL3.0  | [Markdown](LICENSE.MD) | [HTML](LICENSE.HTML) | [Text](LICENSE) | [GNU Website](https://www.gnu.org/licenses/agpl-3.0.html) |
| Releases | [Gitlab](https://gitlab.com/TStudios/roconfirmation/-/releases) |
0J3_0 (The Creator) | [Gitlab](https://gitlab.com/TStudios) | [Roblox](https://www.roblox.com/users/137413810/profile) | [Discord](https://discord.gg/KG2mauA) |


### Links - Raw Text Form
AGPL3.0 | [Markdown](LICENSE.MD) | [HTML](LICENSE.html) | [Text](LICENSE) | [Gnu Website](https://www.gnu.org/licenses/agpl-3.0.html)

Releases | [Gitlab](https://gitlab.com/TStudios/roconfirmation/-/releases)

0J3_0 (The Creator) | [Gitlab](https://gitlab.com/TStudios) | [Roblox](https://www.roblox.com/users/137413810/profile) | [Discord](https://discord.gg/KG2mauA) |